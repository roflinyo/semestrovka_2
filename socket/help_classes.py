class OrderedSet:

    def __init__(self) -> None:
        self.set = []

    def add(self, item):
        if item not in self.set:
            self.set.append(item)

    def remove(self, item):
        self.set.remove(item)
    
