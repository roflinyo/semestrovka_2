from threading import Thread
from help_classes import OrderedSet
from protokol import Protocol
from clientconnection import ClConnection
import pickle

BUFFER = 1024

class PreRoom:
    def __init__(self) -> None:
        self.list_of_players = OrderedSet()
        self.id = None
        self.client_id = 0

    def set_id(self, id):
        self.id = id
        
    def add_player(self, client:ClConnection):
        self.list_of_players.add(client)
        client.id_in_room = self.client_id
        self.client_id += 1

    def send_all(self):
        for client in self.list_of_players.set:
            client.connection.send(
                Protocol.generate_ser_proto("ans", "тестовая рассылка")
            )

    def send(self, client, data):
        client.connection.send(Protocol.generate_proto(data))

    def __str__(self) -> str:
        return "Владелец:" + str(self.id)