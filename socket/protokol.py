import pickle

class Protocol:
    def generate_proto(command, data):
        protocol = {"command": command, "text": data}
        return pickle.dumps(protocol)