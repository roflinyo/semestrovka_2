import pickle
from protokol import Protocol
BUFFER = 1024

class ClConnection:
    def __init__(self, addres, connection) -> None:
        self.address = addres
        self.connection = connection
        self.stil_connect = True
        self.on_server = True
        self.status = None
        self.id_in_room = None
        self.name = None
        self.room_stil_connect = False

    def __str__(self) -> str:
        return str(self.address)
        
    def on_server_off(self):
        self.on_server = False

    def recieve(self):
        return pickle.loads(self.connection.recv(BUFFER))

    def send(self, data, text=""):
        self.connection.send(Protocol.generate_proto(data, text))