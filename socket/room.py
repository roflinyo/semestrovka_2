
from threading import Thread
from clientconnection import ClConnection
from queue import Queue

BUFFER = 1024

class RoomL:
    def __init__(self, list_of_players) -> None:
        print(list_of_players)
        self.list_of_players = list_of_players
        self.queue = Queue()
        self.start_loop(self.list_of_players)

    def start_loop(self, lst):
        for player in lst:
            player.room_stil_connect = True
            thread = Thread(target=self.session, args=(player,))
            thread.start()
        self.queue_treatment()


    def session(self, player:ClConnection):
        while player.room_stil_connect:
            self.room(player)


    def room(self, player:ClConnection):
        resp = player.recieve()
        #print(resp["text"])
        if resp["command"] == "pos":
            self.queue.put(resp["text"])
        

    def mass_send(self, data):
        for player in self.list_of_players:
            player.send("pos", data)

    def mas_send_end_game(self, ind):
        for player in self.list_of_players:
            player.send("game_over", ind)
        
    def queue_treatment(self):
        while True:
            data = self.queue.get()
            if data[1][2] > 12:
                self.mas_send_end_game(data[0])
                break
            self.mass_send(data)

