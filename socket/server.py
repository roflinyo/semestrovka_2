import socket
from threading import Thread
import pickle
from protokol import Protocol
from pre import PreRoom
from multiprocessing import Process
from clientconnection import ClConnection
from room import RoomL

BUFFER = 1024

class Server:
    def __init__(self, ip, port):
        self.clients = set()
        self.sock = socket.socket()
        self.sock.bind((ip, port))
        self.max_connections = 2
        self.list_of_rooms = []
        self.create_rooms()
        self.listen()

  
    def create_rooms(self):
        for i in range(1):
            room = PreRoom()
            room.set_id(i)
            self.list_of_rooms.append(room)

    def listen(self):
        print("Start")
        self.sock.listen(2)
        while True:
            connection, address = self.sock.accept()

            client: ClConnection = ClConnection(address, connection)
            self.clients.add(client)
            print(f"Connected {address}")
            thread = Thread(target=self.start_loop, args=(client,))
            thread.start()

    def start_loop(self, client: ClConnection):
        self.send(client, "name_req")    
        self.session(client)

    def session(self, client: ClConnection):
        while True:
            while client.on_server:
                self.consol(client)
            if not client.stil_connect:
                self.close_client(client)
                break

    def connect_to_room(self, client: ClConnection, room_id):
        room: PreRoom = self.list_of_rooms[room_id - 1]
        room.add_player(client)
        self.send(client, "sucsess")
        self.send(client, "sucsess")
        self.send(client, "sucsess")
        while True:
            resp = client.recieve()
            print(resp["command"])
            if resp["command"] == "want_id":
                self.send(client, "give_id", client.id_in_room)
                self.send(client, "give_id", client.id_in_room)
                self.send(client, "give_id", client.id_in_room)
                break
            
        if len(room.list_of_players.set) == 2:
            self.start_room_process(room)

    def consol(self, client: ClConnection):
        resp = client.recieve()
        if resp["command"] == "exit":
            client.stil_connect = False
            client.on_server = False
            self.send(client, "exit")
        elif resp["command"] == "name_resp":
            client.name = resp["text"]
            self.send(client, "allow")
        elif resp["command"] == "cr_room":
            self.create_room(client)
        elif resp["command"] == "rooms":
            self.send(client, "rooms", data=self.inf_about_room())
        elif resp["command"] == "room_con":
            self.send(client, "sucsess")
            self.connect_to_room(client, resp["text"])

    def start_room_process(self, room:PreRoom):
        proc = Process(target=self.create_room, args=(room,))
        proc.start()
        for player in room.list_of_players.set:
            player.on_server = False
            self.send(player, "start")

    def create_room(self, room:PreRoom):
        RoomL(room.list_of_players.set)

    def inf_about_room(self):
        lst = []
        for room in self.list_of_rooms:
            lst.append(len(room.list_of_players.set))
        return lst

    def disconnect_clients(self, list_of_clients):
        for client in list_of_clients:
            client.on_server_off()


    def send(self, client:ClConnection, command, data=""):
        print(command)
        client.send(command, data)

    def close_client(self, client):
        self.clients.remove(self)
        client.connection.close()


if __name__ == '__main__':
    server = Server("localhost", 8081)
