class Player:
    def __init__(self, x, y, c, color):
        self.x = x
        self.y = y
        self.c = c
        self.color = color

    def get_data(self):
        return (self.x, self.y, self.c)
