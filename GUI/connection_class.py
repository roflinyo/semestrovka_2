import socket
import pickle
from protokol import Protocol

BUFFER = 1024


class ServerConnection:
    def __init__(self, ip, port) -> None:
        self.ip = ip
        self.port = port
        self.id_in_room = None

    def connect(self):
        self.sock = socket.socket()
        self.sock.connect((self.ip, self.port))

    def recieve(self):
        return pickle.loads(self.sock.recv(BUFFER))

    def send(self, data, text=""):
        self.sock.send(Protocol.generate_proto(data, text))
