import random
import threading
from time import sleep
import pickle
import sys
import socket
import numpy as np
from PyQt5 import QtWidgets
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QApplication, QMainWindow, QPushButton, QDialog
from PyQt5 import uic
from PyQt5.QtCore import QThread, pyqtSignal, pyqtSlot, Qt
from player import Player
from connection_class import ServerConnection
from threading import Thread
from maze import maze_generate, present

SIZE_OF_PART = 1024


class ConnectionSend(QThread):
    sendPlayerObject = pyqtSignal(object)

    def __init__(self, player, connection):
        super().__init__()

        self.connection = connection
        self.player = player

    def send(self):
        self.connection.send("pos", (self.connection.id_in_room, self.player.get_pos()))
        sleep(0.2)

    def run(self):
        while True:
            self.send()


class ConnectionReс(QThread):
    sendPlayerObject = pyqtSignal(object)
    endGame = pyqtSignal(object)

    def __init__(self, connection):
        super().__init__()
        self.connection = connection

    def recv(self):
        resp = self.recieve()

    def run(self):
        while True:
            self.recv()

    def recieve(self):
        data = self.connection.recieve()
        if data["command"] == "pos":
            self.sendPlayerObject.emit(data["text"])
        elif data["command"] == "game_over":
            self.endGame.emit(data["text"])


class LoginScreen(QDialog):
    def __init__(self, connection):
        super(LoginScreen, self).__init__()
        self.connection = connection
        self.recieve_server_answer()
        self.load_cscreen()

    def recieve_server_answer(self):
        while True:
            data = self.connection.recieve()
            if data["command"] == "name_req":
                break

    def load_cscreen(self):
        uic.loadUi("loginscreen.ui", self)
        self.accept.clicked.connect(self.login_function)
        self.accept.clicked.connect(self.get_lobby)

    def get_lobby(self):
        widget.setCurrentIndex(widget.currentIndex() + 1)

    def login_function(self):
        name = self.entering_name.text()
        self.connection.send("name_resp", name)
        all_names = []
        if name:
            all_names.append(name)
        print(all_names)


class LobbyScreen(QDialog):
    def __init__(self, connection):
        super(LobbyScreen, self).__init__()
        uic.loadUi("lobby_screen.ui", self)
        self.connection = connection
        self.get_lobbi_inf()
        self.generate_lobbies()

    def get_lobbi_inf(self):
        while True:
            self.connection.send("rooms")
            data = self.connection.recieve()
            if data["command"] == "rooms":
                self.lobie_inf = data["text"]
                break

    def generate_lobbies(self):
        self.num = 1
        self.lobby_size = (1, 1)
        self.btns = np.zeros(self.lobby_size, dtype=QtWidgets.QPushButton)
        for i in range(self.lobby_size[0]):
            for j in range(self.lobby_size[1]):
                btn = QtWidgets.QPushButton(
                    f"Комната - {self.num} ({self.lobie_inf[j]}/2)", self.lobby_frame
                )
                btn.setGeometry(i * 500, j * 70, 550, 50)
                btn.setStyleSheet(f"background-color: white")
                self.btns[i][j] = btn
                self.num += 1
                btn.clicked.connect(lambda: self.game_window(j))
        self.show()

    def game_window(self, ind):
        self.connection.send("room_con", 0)
        while True:
            resp = self.connection.recieve()
            if resp["command"] == "sucsess":
                break
        while True:
            self.connection.send("want_id")
            resp = self.connection.recieve()
            if resp["command"] == "give_id":
                self.connection.id_in_room = resp["text"]
                break
        print("успешное присоеденение")
        widget.setCurrentIndex(widget.currentIndex() + 1)


class WaitWindow(QMainWindow):
    def __init__(self, connection):
        super(WaitWindow, self).__init__()
        uic.loadUi("waitroom.ui", self)
        self.connection = connection
        self.start_wait()

    def start_wait(self):
        thread = Thread(target=self.waiting)
        thread.start()

    def waiting(self):
        while True:
            data = self.connection.recieve()
            if data["command"] == "start":
                break
        self.game()

    def game(self):
        ex.init_players()
        widget.setCurrentIndex(widget.currentIndex() + 1)


mine_percent = 0.21


class GameWindow(QMainWindow):
    keyPressedSignal = pyqtSignal(int)

    def __init__(self, connection):
        super().__init__()
        self.connection = connection
        self.game_size = (15, 15)
        self.key_pressed_count = 0
        self.character = 0

        self.keyPressedSignal.connect(self.moving_by_key)

        self.init_gui()

    def init_players(self):
        print(self.connection.id_in_room)
        if self.connection.id_in_room == 0:
            self.player = Player(0, 0, 0, "green")
            self.another_player = Player(14, 14, 0, "yellow")
        else:
            self.player = Player(14, 14, 0, "yellow")
            self.another_player = Player(0, 0, 0, "green")
        self.establish_connection_recv()

    def init_gui(self):
        uic.loadUi("game_window.ui", self)

        lst_of_walls = maze_generate()
        presents = present()

        self.array_for_maz = []

        self.array_of_presents = []

        self.under_the_btn = np.zeros(self.game_size, dtype=QtWidgets.QStyle)

        self.present = np.zeros(self.game_size, dtype=QtWidgets.QStyle)

        self.btns = np.zeros(self.game_size, dtype=QPushButton)

        self.open = np.zeros(self.game_size, dtype=bool)

        for i in range(self.game_size[0]):
            for j in range(self.game_size[1]):
                btn = QPushButton(f"", self.game_frame)
                btn.setGeometry(i * 50, j * 50, 50, 50)
                if lst_of_walls[i][j] == 0 and presents[i][j] == 0:
                    btn.setStyleSheet("background-color: gray")
                    self.game_frame.setStyleSheet("background-color: gray;")
                    btn.clicked.connect(
                        lambda state, obj=btn, x=i, y=j: self.button_pushed(obj, x, y)
                    )
                else:
                    if lst_of_walls[i][j] != 0:
                        btn.setStyleSheet("background-color: white;")
                        self.array_for_maz.append(btn)
                        btn.clicked.connect(
                            lambda state, obj=btn, x=i, y=j: self.button_pushed(
                                obj, x, y
                            )
                        )
                    if presents[i][j] != 0:
                        btn.setIcon(QIcon("coin.jpg"))
                        self.array_of_presents.append(btn)
                        btn.clicked.connect(
                            lambda state, obj=btn, x=i, y=j: self.button_pushed(
                                obj, x, y
                            )
                        )
                self.btns[i][j] = btn
        self.btns[0][0].setStyleSheet("background-color: green;")
        self.btns[14][14].setStyleSheet("background-color: yellow;")
        self.show()

    def establish_connection_send(self):
        self.parallel_send = ConnectionSend(self.player, self.connection)
        self.parallel_send.start()

    def establish_connection_recv(self):
        self.parallel_recv = ConnectionReс(self.connection)
        self.parallel_recv.start()

        self.parallel_recv.sendPlayerObject.connect(self.player_draw)
        self.parallel_recv.endGame.connect(self.end_game)

    def player_draw(self, data):
        print(data)
        if True:
            if data[0] == self.connection.id_in_room:
                self.change_player_pos(self.player, data[1], self.player.color)
            else:
                if data[2]:
                    self.change_player_pos(
                        self.another_player,
                        data[1],
                        self.another_player.color,
                        data[2]
                    )
                else:
                    self.change_player_pos(
                        self.another_player,
                        data[1],
                        self.another_player.color
                    )

    def end_game(self, ind):
        if ind == 0:
            QtWidgets.QMessageBox.about(self, 'Game over',
                                                    f"Winner is player with green color")
        else:
            QtWidgets.QMessageBox.about(self, 'Game over',
                                                    f"Winner is player with yellow color")


    def change_player_pos(self, player, data, color, coin_coord=None):
        btn = self.btns[player.x][player.y]
        btn.setStyleSheet("background-color: gray")
        if coin_coord:
            btn.setIcon(QIcon())
            if btn in self.array_of_presents:
                self.array_of_presents.remove(btn)
        player.x = data[0]
        player.y = data[1]
        btn = self.btns[player.x][player.y]
        btn.setStyleSheet(f"background-color: {color}")

    def keyPressEvent(self, event):
        self.keyPressedSignal.emit(event.key())

    @pyqtSlot(int)
    def moving_by_key(self, key):

        self.open[self.player.x][self.player.y] = False
        btn = self.btns[self.player.x][self.player.y]

        btn_to_send = None

        if key == Qt.Key_S:
            if (
                    0 <= self.player.y < 14
                    and self.btns[self.player.x][self.player.y + 1]
                    not in self.array_for_maz
            ):
                self.player.y += 1
                if btn in self.array_of_presents:
                    btn.setIcon(QIcon())
                    btn_to_send = (self.player.x, self.player.y)
                    self.array_of_presents.remove(btn)
                    self.player.c += 1
        if key == Qt.Key_W:
            if (
                    0 < self.player.y < 15
                    and self.btns[self.player.x][self.player.y - 1]
                    not in self.array_for_maz
            ):
                self.player.y -= 1
                if btn in self.array_of_presents:
                    btn.setIcon(QIcon())
                    btn_to_send = (self.player.x, self.player.y)
                    self.array_of_presents.remove(btn)
                    self.player.c += 1
        if key == Qt.Key_D:
            if (
                    0 <= self.player.x < 14
                    and self.btns[self.player.x + 1][self.player.y]
                    not in self.array_for_maz
            ):
                self.player.x += 1
                if btn in self.array_of_presents:
                    btn.setIcon(QIcon())
                    btn_to_send = (self.player.x, self.player.y)
                    self.array_of_presents.remove(btn)
                    self.player.c += 1
        if key == Qt.Key_A:
            if (
                    0 < self.player.x < 15
                    and self.btns[self.player.x - 1][self.player.y]
                    not in self.array_for_maz
            ):
                self.player.x -= 1
                if btn in self.array_of_presents:
                    btn.setIcon(QIcon())
                    btn_to_send = (self.player.x, self.player.y)
                    self.array_of_presents.remove(btn)
                    self.player.c += 1

        if self.under_the_btn[self.player.x][self.player.y] == "*":
            btn.setStyleSheet(
                "background-color: gray; font-size: 20px; font-weight: bold"
            )

        else:
            btn.setStyleSheet(
                "background-color: gray; font-size: 20px; font-weight: bold"
            )

        self.open[self.player.x][self.player.y] = True
        btn = self.btns[self.player.x][self.player.y]

        self.connection.send("pos", (self.connection.id_in_room,
                                     self.player.get_data(),
                                     btn_to_send))
        sleep(0.1)

    def button_pushed(self, btn, x, y):
        self.open[self.player.x][self.player.y] = True


if __name__ == "__main__":
    connection = ServerConnection("10.17.64.39", 8081)
    connection.connect()
    app = QApplication(sys.argv)
    widget = QtWidgets.QStackedWidget()
    login_wind = LoginScreen(connection)
    lobby_wind = LobbyScreen(connection)
    wait_room = WaitWindow(connection)
    ex = GameWindow(connection)
    # widget.addWidget(ex)
    widget.addWidget(login_wind)
    widget.addWidget(lobby_wind)
    widget.addWidget(wait_room)
    widget.addWidget(ex)
    widget.setFixedWidth(1121)
    widget.setFixedHeight(821)
    widget.show()
    sys.exit(app.exec_())
