import pickle


class Protocol:
    def generate_proto(data, text):
        protocol = {"command": data, "text": text}

        return pickle.dumps(protocol)

    def generate_ser_proto(command, data):
        protocol = {"body": {"command": command, "text": data}}
        return pickle.dumps(protocol)
